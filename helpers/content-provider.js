// Pure deterministic function
let contentProvider = (seed, source) => {
  const rand = Math.floor(seed * source.length + 1);
  const randomTextContent = source.find(content => content.id === rand);
  return randomTextContent.content;
};

// Currying function with side effects of random
let randomContentProvider = source => contentProvider(Math.random(), source);

module.exports = randomContentProvider;
