# Keyboard Races

Node.js Express server implementation using Socket.io and Passport.js + JWT.
Second and Third task solution for Binary Studio Academy 2019 🎉

## How To

1. Login (user credentials can be found in file `/data/user-credentials.json`)

[![image.png](https://i.postimg.cc/wB08XX6W/image.png)](https://postimg.cc/KKkHFg3L)

2. Wait until you race starts

[![image.png](https://i.postimg.cc/7PV9xqHy/image.png)](https://postimg.cc/NLKmDq2N)

3. Type text, according to the given one in the rectangular, in your text-area as quickly as you can
4. Try not to make typos
5. Just enjoy the game 🎉

[![image.png](https://i.postimg.cc/vTYNZ6rz/image.png)](https://postimg.cc/Q9zb6Cn9)

## Implementation

1. Node.js + Express for the server
2. Passport.js + JWT for authentication
3. Web Sockets implemented using Socket.io
4. HTML/CSS + Bootstrap frontend

## Patterns

1. Facade `/model/commenter.js`
2. Factory `/model/commentFactory.js`
3. Proxy `/model/commenter.js`

## FP

1. `/helpers/content-provider.js`
2. `race.js`
