const jwt = localStorage.getItem("jwt");

if (!jwt) {
  location.replace("/login");
} else {
  const path = "http://localhost:3000";
  const socket = io.connect(path);

  //identify user
  socket.emit("auth", { token: jwt });

  socket.on("game-start", payload => {
    const commentPlaceholder = document.getElementById("comment-placeholder");
    //set greeting comment
    commentPlaceholder.innerHTML = payload.greetingComment;

    //add players' progress-bars
    const newPlayerLable = document.createElement("span");
    newPlayerLable.innerHTML = payload.user;

    const newProgressBar = document.createElement("progress");
    newProgressBar.className = "race-progress";
    newProgressBar.setAttribute("value", "0");
    newProgressBar.setAttribute("max", "100");

    const newPBContainer = document.createElement("div");
    newPBContainer.classList.add("indicator-wrapper");
    newPBContainer.append(newPlayerLable, newProgressBar);

    document
      .getElementById("indicator-placeholder")
      .appendChild(newPBContainer);

    //get random text from the server
    const textContainer = document.getElementById("race-text");
    const textInput = document.getElementById("race-input");
    const raceText = payload.text;

    //set up countdown
    socket.on("countdown", payload => {
      const timeTitle = document.getElementById("time-value-title");
      timeTitle.innerHTML = `The next race starts in: ${payload.countdown}s`;

      let gameTimer = 0;

      if (payload.countdown == 0) {
        textInput.addEventListener("input", handleInput);
        //set random text from the server
        textContainer.innerHTML = raceText;
        //set up game timer
        socket.on("game-timer", () => {
          timeTitle.innerHTML = `${gameTimer++}s`;
        });

        setInterval(() => {
          socket.emit("game-status", { result: newProgressBar.value });
        }, 30000);
      }
    });

    socket.on("show-random-comments", payload => {
      commentPlaceholder.innerHTML = payload.randomComment;
    });

    socket.on("show-game-status-comments", payload => {
      commentPlaceholder.innerHTML = payload.gameStatusComment;
    });

    //handles textarea input
    function handleInput() {
      let userInput = [...textInput.value];
      let serverText = [...raceText];
      let i = 0;

      //highlight correct user input
      let correctInput = userInput.filter(el => el == serverText[i++]);

      const MAX_PB_VALUE = 100;
      //count percent of progress
      const pbValue = (MAX_PB_VALUE * correctInput.length) / serverText.length;
      newProgressBar.setAttribute("value", Math.floor(pbValue));

      if (serverText.length - correctInput.length == 30) {
        //set finish-line comment when there are 30 letters before the end
        socket.on("show-finishline-comment", payload => {
          commentPlaceholder.innerHTML = payload.finishLineComment;
        });
      }

      if (serverText.length - correctInput.length == 1) {
        socket.on("show-winner", payload => {
          //remove race-text from the container
          document.getElementById("race-text").innerHTML = "";
          //set comment, announcing winner
          document.getElementById("comment-placeholder").innerHTML =
            payload.finishComment;

          setTimeout(() => {
            location.reload();
          }, 5000);
        });
      }

      textContainer.innerHTML = [
        correctInput
          .map(
            el =>
              `<span style="background-color: green; text-style: bold;">${el}</span>`
          )
          .join(""),
        serverText.join("").substr(i)
      ].join("");
    }
  });

  socket.on("join-user", payload => {
    document.getElementById("game-info-placeholder").innerHTML += ` <b>${
      payload.user
    }</b> has been connected <br/>`;
  });

  socket.on("disconnect-user", payload => {
    document.getElementById("game-info-placeholder").innerHTML += `<b>${
      payload.user
    }</b> has been disconnected <br/>`;
  });
}

const logoutBtn = document.getElementById("logout-btn");
logoutBtn.addEventListener("click", e => {
  localStorage.removeItem("jwt");
  location.replace("/");
});
