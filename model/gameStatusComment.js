class GameStatusComment {
  constructor() {
    this.text = "Game status for ";
  }

  constructComment(player) {
    return `${this.text} <b>${player.username}</b> <br> <b>Result: ${
      player.result
    }</b> &#127881;`;
  }
}

module.exports = GameStatusComment;
