const greetingComments = require("../data/greeting-comments.json");
const cars = require("../data/cars.json");
const contentProvider = require("../helpers/content-provider");

class GreetingComment {
  constructor() {
    this.text = contentProvider(greetingComments);
  }

  constructComment(player) {
    return `&#127881; &#127881; &#127881;${this.text} <b>${
      player.username
    }</b>. My name is <b>${
      player.commentorName
    }</b>. I will comment your race! Your car for this race is <b>${(player.vehicle = contentProvider(
      cars
    ))}</b> 	&#x1f697;`;
  }
}

module.exports = GreetingComment;
