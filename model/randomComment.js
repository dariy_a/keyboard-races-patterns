const randomComments = require("../data/random-comments.json");
const contentProvider = require("../helpers/content-provider");

class RandomComment {
  constructor() {
    this.text = contentProvider(randomComments);
  }

  constructComment(player) {
    return `<b>${player.username}</b>, ${this.text}`;
  }
}

module.exports = RandomComment;
