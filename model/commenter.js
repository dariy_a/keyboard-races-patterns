const commenterData = require("../data/commentors.json");
const contentProvider = require("../helpers/content-provider");
const CommentFactory = require("./commentFactory");
const Player = require("./player");

//Facade
class Commenter {
  constructor(playerName) {
    this.username = contentProvider(commenterData);
    this.commentFactory = new CommentFactory();

    //Proxy Pattern
    let proxyPlayer = new Proxy(new Player(playerName), {
      get(target, prop) {
        return target[prop];
      },
      set(target, prop, value) {
        target[prop] = value;
        return true;
      }
    });

    this.player = proxyPlayer;
  }

  greetings() {
    this.player.commentorName = this.username;
    const comment = this.commentFactory
      .createComment("greeting")
      .constructComment(this.player);
    return comment;
  }

  status(result) {
    this.player.result = result;
    const comment = this.commentFactory
      .createComment("status")
      .constructComment(this.player);
    return comment;
  }

  randomSpeech() {
    const comment = this.commentFactory
      .createComment("random")
      .constructComment(this.player);
    return comment;
  }

  finishLine() {
    const comment = this.commentFactory
      .createComment("finishline")
      .constructComment(this.player);
    return comment;
  }

  finish() {
    const comment = this.commentFactory
      .createComment("finish")
      .constructComment(this.player);
    return comment;
  }
}

module.exports = Commenter;
