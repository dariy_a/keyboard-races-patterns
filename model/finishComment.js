class FinishComment {
  constructor() {
    this.text = "The winner is";
  }

  constructComment(player) {
    return `${this.text} &ndash; <b>${player.username}</b>`;
  }
}

module.exports = FinishComment;
