const GreetingComment = require("./greetingComment.js");
const GameStatusComment = require("./gameStatusComment.js");
const FinishComment = require("./finishComment.js");
const FinishLineComment = require("./finishLineComment.js");
const RandomComment = require("./randomComment.js");

//Factory
class CommentFactory {
  createComment(type) {
    switch (type) {
      case "greeting":
        return new GreetingComment();
      case "status":
        return new GameStatusComment();
      case "finishline":
        return new FinishLineComment();
      case "finish":
        return new FinishComment();
      case "random":
        return new RandomComment();
      default:
        console.error("undefined comment type");
    }
  }
}

module.exports = CommentFactory;
