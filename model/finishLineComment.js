class FinishComment {
  constructor() {
    this.text = "There are 30 letters left to finish";
  }

  constructComment(player) {
    return `${this.text} &ndash; <b>${
      player.username
    }</b> shows excellent results!`;
  }
}

module.exports = FinishComment;
