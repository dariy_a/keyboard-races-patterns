const path = require("path");
const logger = require("morgan");
const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const jwt = require("jsonwebtoken");
const passport = require("passport");

const appRouter = require("./routes/routes");
const textes = require("./data/race-textes.json");
const contentProvider = require("./helpers/content-provider");
const Commenter = require("./model/commenter");

require("./middlewares/passport.config");

app.use("/", appRouter);
app.use("/login", appRouter);

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());

server.listen(3000);

let roomNumber = 1;

io.sockets.on("connection", socket => {
  //increase room number when 2 clients are present in a room
  if (
    io.nsps["/"].adapter.rooms["room-" + roomNumber] &&
    io.nsps["/"].adapter.rooms["room-" + roomNumber].length > 2
  ) {
    roomNumber++;
  }

  socket.on("auth", payload => {
    socket.join("room-" + roomNumber);
    //identify user
    const username = jwt.decode(payload.token).username;

    //set up countdown before the game
    let countdown = 10;
    let timerId = setInterval(() => {
      if (countdown) {
        countdown--;
      } else {
        clearInterval(timerId);
      }
      socket.emit("countdown", { countdown: countdown });
    }, 1000);

    //send greeting comment
    const greetingComment = new Commenter(username).greetings();

    //get random text from the file
    const randomText = contentProvider(textes);

    socket.emit("game-start", {
      user: username,
      text: randomText,
      greetingComment: greetingComment
    });

    io.in(`room-${roomNumber}`).emit("join-user", { user: username });

    //set up game timer
    setInterval(() => {
      socket.emit("game-timer");
    }, 1000);

    socket.on("game-status", payload => {
      let gameResult = payload.result;

      const gameStatusComment = new Commenter(username).status(gameResult);

      //send comment with player's points
      socket.emit("show-game-status-comments", { gameStatusComment });
    });

    //send random comment
    setInterval(() => {
      const randomComment = new Commenter(username).randomSpeech();
      socket.emit("show-random-comments", { randomComment });
    }, 10000);

    //send finish-line comment when there are 30 letters before the end
    const finishLineComment = new Commenter(username).finishLine();

    //send finish comment when the player typed all the letters
    const finishComment = new Commenter(username).finish();

    setInterval(() => {
      socket.emit("show-finishline-comment", { finishLineComment });
      io.in(`room-${roomNumber}`).emit("show-winner", {
        finishComment
      });
    }, 100);

    socket.on("disconnect", () => {
      io.in(`room-${roomNumber}`).emit("disconnect-user", { user: username });
    });
  });
});

module.exports = app;
